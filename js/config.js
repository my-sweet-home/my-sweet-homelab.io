window.gtpb = window.gtpb || {};



     

     


gtpb.default.site_id = "gt";
gtpb.default.SiteMenu = {"global":[{"URL":"/","Page":null,"Name":"Home","Menu":"global","Identifier":"tutorials","Pre":"","Post":"","Weight":1,"Parent":"","Children":null},{"URL":"/about/","Page":null,"Name":"About","Menu":"global","Identifier":"about","Pre":"","Post":"","Weight":10,"Parent":"","Children":null}],"iconmenu":[{"URL":"https://gitlab.com/my-sweet-home/my-sweet-home.gitlab.io","Page":null,"Name":"Fork Website Repo","Menu":"iconmenu","Identifier":"","Pre":"Fork Me","Post":"","Weight":1,"Parent":"","Children":null},{"URL":"https://www.patreon.com/goandmake","Page":null,"Name":"Sponsor on Patreon","Menu":"iconmenu","Identifier":"","Pre":"Sponsor","Post":"","Weight":10,"Parent":"","Children":null}]};
gtpb.default.SiteParams = {"absolutebuilderurl":true,"accent_color":"hsl(10,39%,45%)","accent_color_light":"hsl(10,39%,65%)","css_url":"https://fonts.googleapis.com/css?family=Courgette","custom_css":".handwriting {\n  font-family: 'Courgette', cursive;\n}","env":"production","flex_box_interior_classes":"w-100 w-50-m w-33-l ","gh_provider":"gitlab","gh_repo_image":["images/architecture-1867187_1920_pixabay.jpg","images/logos/icon-on-dark.svg","images/logos/icon-on-light.svg","images/logos/logo.svg"],"gh_repo_name":"my-sweet-home.gitlab.io","gh_repo_owner":"my-sweet-home","global_section":{},"gt_author_name":"GoAndMake.app","gt_author_url":"https://we.goandmake.app","gt_license_name":"CC-BY-NC-4.0","gt_license_url":"https://creativecommons.org/licenses/by-nc/4.0/","gt_theme_name":"My Sweet Home","header_class":"bg-primary-color","js_export":{},"js_partial":{},"lightness":45,"lightness_dark":20,"lightness_light":65,"lightness_lighter":80,"logo_url":"/images/logos/icon-on-dark.svg","mainSections":["post"],"main_menu_align":"start","mainsections":["post"],"mobile_menu_button":[{"label":"Menu","menu":"global"},{"label":"Account","menu":"iconmenu"}],"primary_hue":85,"saturation":35,"site_logo_class":"w-100 tc pb3","skip_cover_nav":false};
gtpb.default.SiteBaseURL = "https://my-sweet-home.gitlab.io";
gtpb.default.SiteCopyright = "Copyright 2018, GoAndMake.app";
gtpb.default.SiteTitle = "My Sweet Home theme demo";
gtpb.default.SiteFooter = {"appearance":{"legal_link_color":"link-white-80","nav_link_color":"link-gold","section_background":"bg-black-80"},"is_global":true,"menu":{"footer_legal":[{"name":"Terms of Service","url":"#"},{"name":"Privacy","url":"#"}],"footer_nav":[{"name":"Home","url":"#"},{"name":"About","url":"#"}]}};
